
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorsService } from 'src/app/authors.service';


@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
    // authors: object[] = [{id:1 ,name:'Lewis Carrol' }, {id:2 , name: 'Leo Tolstoy'}, {id:3 ,name:'Thomas Mann'}];
    constructor(private route: ActivatedRoute,private authorsservice:AuthorsService) { }

    author; 
    id;
    newAuthor:string;
    authors:any;
  
    authors$:Observable<any>;
  
    addNewAuthor(){
      this.authorsservice.addAuthors(this.newAuthor);
    }
  
    ngOnInit() {
      this.authors$ = this.authorsservice.getAuthors();
      this.author = this.route.snapshot.params.author;
      this.id = this.route.snapshot.params.id;
    }
 
}
