import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private authService:AuthService,
    private router:Router) { }

email:string;
password:string; 

onSubmit(){
this.authService.signUp(this.email,this.password);
this.router.navigate(['/posts']);
}

ngOnInit() {
}

}
