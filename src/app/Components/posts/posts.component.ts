
import { Component, OnInit } from '@angular/core';
// import { BlogPost } from './../interfaces/blog-post';
// import { User } from './../interfaces/user';
import { Observable } from 'rxjs';
import { PostsService } from 'src/app/posts.service';
import { Post } from 'src/app/interfaces/Post';
import { User } from 'src/app/interfaces/User';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
  // Posts$
  Posts$:Observable<any[]>;
  // Users$
  
 // //For load to firebase
  // Posts$: Post[]=[];
  // Users$: User[]=[];
  // title:string; 
  // body:string;
  // author:string;
  // message:String;
  userId:string;
  constructor(private postsservice: PostsService ,public authService:AuthService) { }

    deletePost(id){
    this.postsservice.deletePost(this.userId,id)
    console.log(id);
   }
  
  // //function who upload in the database all the posts
  // saveFunc(){
  //   for (let index = 0; index < this.Posts$.length; index++) {
  //     for (let i = 0; i < this.Users$.length; i++) {
  //       if (this.Posts$[index].userId==this.Users$[i].id) {
  //         this.title = this.Posts$[index].title;
  //         this.body = this.Posts$[index].body;
  //         this.author = this.Users$[i].name;
  //         this.postsservice.addPost(this.title , this.body, this.author);
          
  //       }
        
        
  //     }
      
  //   }
  //   this.message ="The data loading was successful"
  // }

  ngOnInit() 
    {
      // //Asyenc show
        // this.Posts$ = this.postsservice.getPosts();
        // this.Users$ = this.postsservice.getUsers();

      //   //For upload to firebase
      //   this.postsservice.getPosts()
      // .subscribe(data =>this.Posts$ = data );
      // this.postsservice.getUsers()
      // .subscribe(data =>this.Users$ = data );
      // 

      // //Authintication show
      console.log("NgOnInit started")  
        this.authService.getUser().subscribe(
          user => {
            this.userId = user.uid;
            this.Posts$ = this.postsservice.getPosts(this.userId); 
          }
       )

    }
}





////#########################################--- Codes -- ########################################
// //Delete
// deletePost(id){
//   this.postsservice.deletePost(id)
//   // this.postsservice.deletePost(id)
//   console.log(id);
//  }